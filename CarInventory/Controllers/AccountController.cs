﻿using CarInventory.DAL;
using CarInventory.Models;
using log4net;
using System.Web.Mvc;

namespace CarInventory.Controllers
{
    public class AccountController : Controller
    {
        CarsContext db = new CarsContext();
        ILog logger = LogManager.GetLogger(typeof(AccountController));

        private IUserRepository userRepository;

        public AccountController()
        {
            userRepository = new UserRepository(new CarsContext());
        }

        public AccountController(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public ActionResult Login()
        {
            if (Session["uid"] != null)
                return RedirectToAction("Index", "Home");
            else
                return View();
        }

        public ActionResult Logout()
        {
            Session.Remove("uid");
            Session.Remove("userName");
            return RedirectToAction("Login", "Account");
        }

        public ActionResult Authorize(Users user)
        {
            Users userDetail = userRepository.GetUser(user.UserName, user.Password);
            if(userDetail != null)
            {
                Session["uid"] = userDetail.Id;
                Session["userName"] = userDetail.UserName;
                return RedirectToAction("Index", "Car");
            }
            return RedirectToAction("Login", "Account");
        }

    }
}