﻿using CarInventory.DAL;
using CarInventory.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CarInventory.Controllers
{
    public class CarController : Controller
    {
        CarsContext db = new CarsContext();
        ILog logger = LogManager.GetLogger(typeof(CarController));

        private ICarsRepository carRepository;

        public CarController()
        {
            this.carRepository = new CarsRepository(new CarsContext());
        }

        public CarController(ICarsRepository carRepository)
        {
            this.carRepository = carRepository;
        }


        public ViewResult Index(string searchString)
        {
            int userId = 0;
            if (Session["uid"] == null)
                Response.Redirect("Account/Login");

            int.TryParse(Convert.ToString(Session["uid"]), out userId);

            try
            {
                var cars = from car in carRepository.GetCars(userId)
                           select car;

                if (!String.IsNullOrEmpty(searchString))
                {
                    cars = cars.Where(s => s.Model.ToUpper().Contains(searchString.ToUpper())
                                           || s.Brand.ToUpper().Contains(searchString.ToUpper()));
                }

                return View(cars);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                return View(new List<Cars>());
            }
        }

        [HttpGet]
        public ActionResult AddEditRecord(int? id)
        {
            try
            {
                if (id != null)
                {
                    ViewBag.IsUpdate = true;
                    Cars car = carRepository.GetCarByID(Convert.ToInt32(id));
                    return PartialView("_CarData", car);
                }
                ViewBag.IsUpdate = false;
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return PartialView("_CarData");
        }

        [HttpPost]
        public ActionResult AddEditRecord(Cars car, string cmd)
        {
            if (ModelState.IsValid)
            {
                if (cmd == "Save")
                {
                    try
                    {
                        carRepository.InsertCar(car);
                        carRepository.Save();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                    }
                }
                else
                {
                    try
                    {
                        carRepository.UpdateCar(car);
                        carRepository.Save();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                    }
                }
            }

            return PartialView("_CarData", car);
        }

        public ActionResult DeleteRecord(int id)
        {
            Cars car = carRepository.GetCarByID(id);
            if (car != null)
            {
                try
                {
                    carRepository.DeleteCar(id);
                    carRepository.Save();
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            try
            {
                Cars car = carRepository.GetCarByID(id);
                if (car != null)
                {
                    return PartialView("_CarDetails", car);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return View("Index");
        }
    }
}