﻿using CarInventory.Models;
using System;
using System.Collections.Generic;

namespace CarInventory.DAL
{
    public interface ICarsRepository : IDisposable
    {
        IEnumerable<Cars> GetCars(int userId);
        Cars GetCarByID(int carId);
        void InsertCar(Cars car);
        void DeleteCar(int carId);
        void UpdateCar(Cars car);
        void Save();
    }
}