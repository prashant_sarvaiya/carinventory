﻿using CarInventory.Models;
using System;
using System.Collections.Generic;

namespace CarInventory.DAL
{
    public interface IUserRepository : IDisposable
    {
        Users GetUser(string userName, string password);
    }
}