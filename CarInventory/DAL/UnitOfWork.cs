﻿using CarInventory.Models;
using System;

namespace CarInventory.DAL
{
    public class UnitOfWork : IDisposable
    {
        private CarsContext context = new CarsContext();
        private GenericRepository<Cars> carsRepository;

        public GenericRepository<Cars> CarsRepository
        {
            get
            {

                if (this.carsRepository == null)
                {
                    this.carsRepository = new GenericRepository<Cars>(context);
                }
                return carsRepository;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}