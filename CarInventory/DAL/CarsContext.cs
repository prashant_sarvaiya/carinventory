﻿using CarInventory.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CarInventory.DAL
{
    public class CarsContext : DbContext
    {
        public DbSet<Cars> Cars { get; set; }
        public DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Configuration.LazyLoadingEnabled = true;
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}