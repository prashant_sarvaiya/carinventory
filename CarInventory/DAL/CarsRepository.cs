﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarInventory.Models;
using System.Data.Entity;

namespace CarInventory.DAL
{
    public class CarsRepository : ICarsRepository, IDisposable
    {
        private CarsContext context;

        public CarsRepository(CarsContext context)
        {
            this.context = context;
        }

        public void DeleteCar(int carId)
        {
            Cars car = context.Cars.Find(carId);
            context.Cars.Remove(car);
        }


        public Cars GetCarByID(int carId)
        {
            return context.Cars.Find(carId);
        }

        public IEnumerable<Cars> GetCars(int userId)
        {
            return context.Cars.Where(c => c.UserId == userId).ToList();
        }

        public void InsertCar(Cars car)
        {
            context.Cars.Add(car);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateCar(Cars car)
        {
            context.Entry(car).State = EntityState.Modified;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}