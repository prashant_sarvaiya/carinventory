﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarInventory.Models;

namespace CarInventory.DAL
{
    public class UserRepository : IUserRepository, IDisposable
    {
        private CarsContext context;

        public UserRepository(CarsContext context)
        {
            this.context = context;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public Users GetUser(string userName, string password)
        {
            return context.Users.Where(c=>c.UserName == userName && c.Password == password).FirstOrDefault();
        }
    }
}