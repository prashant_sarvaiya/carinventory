﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarInventory.Models
{
    [Table("Cars")]
    public class Cars
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }

        [Required]
        public decimal Price { get; set; }
        public bool New { get; set; }
        public int UserId { get; set; }
    }
}